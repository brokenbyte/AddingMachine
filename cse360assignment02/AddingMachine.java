package cse360assignment02;

public class AddingMachine {
  private int total;            // The current AddingMachine's total
  private StringBuffer history; // Transaction history for the AddingMachine

  /**
   * Initialize an Adding Machine
   *
   */
  public AddingMachine() {
    total = 0;  // not needed - included for clarity
    history = new StringBuffer("0");
  }

  /**
   * Get the current total of the AddingMachine
   *
   * @return The current total of the AddingMachine
   *
   */
  public int getTotal() {
    return total;
  }

  /**
   * Add a number to the total
   *
   * @param value The value to add
   *
   */
  public void add(int value) {
    total += value;
    history.append(" + " + value);
  }

  /**
   * Subtract a number from the total
   *
   * @param value The value to subtract
   *
   */
  public void subtract(int value) {
    total -= value;
    history.append(" - " + value);
  }

  /**
   * Returns the transaction history for the adding machine
   *
   * @return String representation of the transaction history
   *
   */
  public String toString() {
    return this.history.toString();
  }

  /**
   * Clear the transaction history and reset the total
   *
   */
  public void clear() {
    history = new StringBuffer("0");
    total = 0;
  }
}
